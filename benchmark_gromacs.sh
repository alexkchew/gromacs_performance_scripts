#!/bin/bash

# benchmark_gromacs.sh
# The purpose of this script is to benchmark the performance of GROMACS. 
# One of the challenges of benchmarking GROMACs is that each system 
# is with a different size and each system may have different simulation
# parameters (e.g. restraints added, etc.). As a result, 
# it is difficult to gauge the performance of GROMACS when attempting 
# to parallelize across multiple nodes. By benchmarking GROMACS to a specific 
# system, we can then generalize how well the job performs for a specific 
# server and GROMACS version. 
# 
# ASSUMPTIONS OF THE READER:
#	1 - You already know how to run GROMACS
#	2 - You already have a system that you are trying to test
#	3 - You need to benchmark the system for a server
# 
# RUN CODE:
#	benchmark_gromacs

# Written by: Alex K. Chew (04/10/2020)


## PRINTING
echo "~~~~~ benchmark_gromacs.sh ~~~~~"

## DEFINING OUTPUT DIRECTORY
output_dir="${1-benchmarking}"

## DEFINING CURRENT INPUT VARIABLES
input_vars_script="${2:-benchmark_input_vars.sh}"

## CHECKING IF INPUT VARS EXISTS
if [ ! -e "${input_vars_script}" ]; then
	echo "Error! The input script for this script is not available."
	echo "Please run 'benchmark_start' command and edit the input variables"
	echo "Stopping here to prevent further errors!"
	exit 1
fi

## PRINTING INPUT VARIABLES
benchmark_print_input_vars

## LOADING SCRIPT
source "${input_vars_script}"

#########################
### DEFAULT VARIABLES ###
#########################
## GETTING SCRIPT PATH
script_path="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

## DEFINING RUN SCRIPT
run_script="run_tpr.sh"
# "run.sh"
path_run_script="${PERF_PATH_TO_BENCHMARK_DIR}/${run_script}"

## DEFINING OUTPUT RUNSCRIPT
output_run_script="run.sh"
output_submit_script="submit.sh"

## DEFAULT MAX WARN
max_warn="5"

######################
### INPUT VARABLES ###
######################

### FILES THAT ARE INPUTTED INTO THE SYSTEM

## DEFINING PATH TO LOOK INTO
path_of_origin="$(pwd)"
tpr_file="${BENCHMARK_TPR_PATH}"
# "/home/akchew/scratch/nanoparticle_project/nplm_sims/20200124-US-sims_NPLM_reverse_stampede/USrev-1.5_5_0.2-pullnplm-1.300_5.000-0.0005_2000-DOPC_196-EAM_2_ROT012_1/4_simulations/5.500/nplm_prod.tpr"

## CHECKING INPUTS
if [ ! -e "${tpr_file}" ]; then 
	echo "Error! TPR file is not found!"
	echo "Please check the path: ${tpr_file}"
	echo "Stopping here to prevent further errors!"
	exit 1
fi

## DEFINING TOTAL SIMULATION TIME
total_sim_time_ps="2000.000"

## DEFINING PATH TO OUTPUT
path_output_dir="${path_of_origin}/${output_dir}"

## DEFINING OUTPUT PREFIX
output_prefix="benchmark"

#############################################################
### PART 1: COPYING ALL NECESSARY FILES FOR A GROMACS JOB ###
#############################################################
echo "------------------------------------------"
echo "STEP 1: COPYING ALL FILES TO OUTPUT FOLDER"
echo "------------------------------------------"

## DEFINING OUTPUT MDP
output_mdp="${output_prefix}_prod.mdp"

## COPYING A LOT OF FILES IF TPR FILE DOES NOT EXIST
cp -r "${tpr_file}" "${output_prefix}.tpr"

# if [ ! -e "${tpr_file}" ]; then ## DEPRECIATED, ASSUME THAT TPR EXISTS. 

# 	## COPYING TOP FILE (AND ALL OTHER TOPOLOGY REQUIREMENTS)
# 	copy_top_include_files "${top_file}" "${path_output_dir}"

# 	## COPYING TOP FILE
# 	cp -r "${top_file}" "${output_prefix}.top"

# 	## COPYING GRO FILE
# 	cp -r "${gro_file}" "${output_prefix}.gro"

# 	## COPYING MDP
# 	cp -r "${mdp_file}" "${output_mdp}"

	
# fi

################################
### PART 2: EDITING TPR FILE ###
################################

echo -e "\n------------------------------------------"
echo "STEP 2: EDITING TPR FILE TO ${BENCHMARK_SIM_TIME_PS} ps"
echo "------------------------------------------"
## CONVERTING THE TPR
${BENCHMARK_GROMACS_COMMAND} convert-tpr -s "${output_prefix}.tpr" \
										 -o "${output_prefix}.tpr" \
										 -until "${BENCHMARK_SIM_TIME_PS}"

## REMOVING EXTRA FILES
rm -rf \#*

#####################################
### PART 3: GENERATING RUN SCRIPT ###
#####################################

echo -e "\n------------------------------------------"
echo "STEP 3: GENERATING RUN SCRIPT"
echo "------------------------------------------"

## PRINTING
echo "Copying default ${run_script} to current directory"
echo "   Output prefix: ${output_prefix}"
# echo "   MDP file: ${output_prefix}_prod.mdp"

## COPYING RUN SCRIPT
cp -r "${path_run_script}" "${output_run_script}"

## EDITING RUN SCRIPT
# sed -i "s#_MDPFILE_#${output_mdp}#g" "${output_run_script}"
sed -i "s#_OUTPUTPREFIX_#${output_prefix}#g" "${output_run_script}"
sed -i "s#_MAXWARN_#${max_warn}#g" "${output_run_script}"

## CHECKING SUBMISSION SCRIPT
if [ ! -e "${path_of_origin}/${BENCHMARK_SUBMIT_SCRIPT}" ]; then
	echo "Error! No submission file has been inputted"
	echo "Current submission script: ${BENCHMARK_SUBMIT_SCRIPT}"
	echo "Check out submission scripts in 'benchmark_list_submit'"
	echo "Copy submission script by using 'benchmark_copy_submit', e.g. 'benchmark_copy_submit submit_SWARM.sh'"
	echo "Update the 'BENCHMARK_SUBMIT_SCRIPT' to the correct submission script, then run 'benchmark_gromacs' function again"
	echo "Stopping here!"
	exit 1
fi

###########################################
### PART 4: ITERATE THROUGH EACH SCRIPT ###
###########################################

## DEFINING JOB LIST
job_list="${path_of_origin}/job_list.txt"

## EMPTYING JOB LIST
> "${job_list}"

## RE-CREATING GROMACS
if [ -e "${path_output_dir}" ]; then
	echo "Note! ${path_output_dir} exists!"
	echo "Re-writing by removing all files from this directory!"
	echo "Pausing for 5 seconds, ctrl + c if you do not want to overwrite these files!"
	sleep 5
	rm -r "${path_output_dir}"
fi

## MAKING DIRECTORY
mkdir -p "${path_output_dir}"

## DEFINING ARRAYS
read -a num_nodes_array <<< "$(str2array_by_delim "${BENCHMARK_NUM_NODES}" ",")"
read -a num_cores_array <<< "$(str2array_by_delim "${BENCHMARK_NUM_CORES}" ",")"

## LOOPING THROUGH EACH NODE
for num_nodes in ${num_nodes_array[@]}; do
	## LOOPING THROUGH EACH CORE
	for num_cores in ${num_cores_array[@]}; do
		## CREATING NAME
		current_name="nodes_${num_nodes}-corespernode_${num_cores}"
		## PRINTING
		echo "--> Creating job for ${current_name}"
		## MAKING DIRECTORY
		path_current_name="${path_output_dir}/${current_name}"
		mkdir -p "${path_current_name}"

		## COPYING TPR FILE AND RUN FILE
		cp -r "${output_run_script}" "${output_prefix}.tpr" "${path_output_dir}/${current_name}"

		## COPYING SUBMISSION FILE
		path_output_submit_script="${path_output_dir}/${current_name}/${output_submit_script}"
		cp -r "${path_of_origin}/${BENCHMARK_SUBMIT_SCRIPT}" "${path_output_submit_script}"

		## COMPUTING NUMBER OF CORES
		total_cores=$(awk -v n_nodes=${num_nodes} \
		   				  -v n_cores=${num_cores} \
			   				'BEGIN{ printf "%d", n_nodes*n_cores }')

		## EDITTING SUBMISSION FILE
		sed -i "s/_JOBNAME_/${current_name}/g" "${path_output_submit_script}"
		sed -i "s/_TIMEALLOTTED_/${BENCHMARK_TIME_ALLOTTED}/g" "${path_output_submit_script}"
		sed -i "s/_NUMNODES_/${num_nodes}/g" "${path_output_submit_script}"
		sed -i "s/_CORESPERNODE_/${num_cores}/g" "${path_output_submit_script}"
		sed -i "s/_TOTALCORES_/${total_cores}/g" "${path_output_submit_script}"
		sed -i "s/_RUNSCRIPT_/${output_run_script}/g" "${path_output_submit_script}"
		sed -i "s/_EMAIL_/${BENCHMARK_EMAIL_ADDRESS}/g" "${path_output_submit_script}"

		## ADDING TO JOB LIST
		echo "./${output_dir}/${current_name}/${output_submit_script}" >> "${job_list}"

	done

done

## PRINTING
echo "===== Complete! ====="
echo "Check the output directory: ${output_dir}"
echo "To submit 1 job, run: benchmark_submit_jobs 1 job_list.txt"
echo "If you want to clean the job list, simply run: > job_list.txt"
echo "After submission and running the code, then perform extraction protocols"




# ########################################
# ### PART 3: EDITING TIME OF MDP FILE ###
# ########################################

# echo -e "\n------------------------------------------"
# echo "STEP 3: EDITING MDP FILE TIME"
# echo "------------------------------------------"

# ## GETTING TIME STEP
# mdp_dt=$(mdp_find_option_values "${output_mdp}" "dt")

# ## COMPUTING NEW TIME
# new_nsteps=$(awk -v dt=${mdp_dt} \
# 			   -v new_time_ps=${total_sim_time_ps} \
# 			   'BEGIN{ printf "%d", new_time_ps / dt}')

# echo "dt: ${mdp_dt}; desired time in ps: ${total_sim_time_ps}; new_nsteps: ${new_nsteps}"

# ## EDITING MDP FILE
# mdp_edit_option_values "${output_mdp}" "nsteps" "${new_nsteps}"



## MAKE EDITABLE -- INCLUSION OF TPR FILE

## TODO: GENERATE RUN SCRIPTS FOR MULTIPLE
## TODO: INCLUSION OF SUBMISSION SCRIPTS








