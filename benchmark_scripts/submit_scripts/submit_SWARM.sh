#!/bin/bash 

# submit_SWARM.sh
# This submission scripts run benchmark umbrella sampling simulations
# VARIABLES TO CHANGE:
#   _JOBNAME_ <-- job name
#   _NUMNODES_ <-- number of nodes
#	_CORESPERNODE_ <-- number of cores per node
#	_EMAIL_ <-- email address (e.g. akchew@wisc.edu)
# 	_TOTALCORES_ <-- total number of cores
#	_RUNSCRIPT_ <-- run script
#	_TIMEALLOTTED_ <-- total time allotted for the job

###SERVER_SPECIFIC_COMMANDS_START

#SBATCH -p compute
#SBATCH -t _TIMEALLOTTED_
#SBATCH -J _JOBNAME_
#SBATCH --nodes=_NUMNODES_
#SBATCH --ntasks-per-node=_CORESPERNODE_
#SBATCH --mail-user=_EMAIL_
#SBATCH --mail-type=all  # email me when the job starts

# ---- RUN SCRIPTS BELOW ----- #

## DEFINING NUMBER OF CORES TO RUN THIS ON
num_cores="_TOTALCORES_" # number of cores you want to run with
mdrun_command="gmx mdrun -nt" # command to run code
mdrun_command_suffix=""

###SERVER_SPECIFIC_COMMANDS_END

## DEFINING RUN SCRIPT
run_script="./_RUNSCRIPT_"

## RUNNING COMMANDS
bash "${run_script}" "${num_cores}" \
                     "${mdrun_command}" \
                     "${mdrun_command_suffix}"
