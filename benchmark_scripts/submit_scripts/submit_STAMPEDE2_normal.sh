#!/bin/bash 

# submit_STAMPEDE2_normal.sh
# This submission scripts run benchmark umbrella sampling simulations
# VARIABLES TO CHANGE:
#   _JOBNAME_ <-- job name
#   _NUMNODES_ <-- number of nodes
#	_CORESPERNODE_ <-- number of cores per node
#	_EMAIL_ <-- email address (e.g. akchew@wisc.edu)
# 	_TOTALCORES_ <-- total number of cores
#	_RUNSCRIPT_ <-- run script
#	_TIMEALLOTTED_ <-- time allotted for the script

###SERVER_SPECIFIC_COMMANDS_START
#SBATCH -p normal
#SBATCH -J _JOBNAME_
#SBATCH --output="slurm.%j.out"
#SBATCH --nodes=_NUMNODES_
#SBATCH --ntasks-per-node=_CORESPERNODE_
#SBATCH --export=ALL
#SBATCH -t _TIMEALLOTTED_
#SBATCH --mail-user=_EMAIL_
#SBATCH --mail-type=all  # email me when the job starts
#SBATCH -A TG-CTS170045

# NEED TO USE IBRUN TI LAUNCH MPI CODES ON TAAC SYSTEM (NOT MPIRUN OR MPIEXEC)

# ---- RUN SCRIPTS BELOW ----- #

## DEFINING NUMBER OF CORES TO RUN THIS ON
num_cores="_TOTALCORES_" # number of cores you want to run with
mdrun_command="ibrun -np " # command to run code
mdrun_command_suffix="mdrun_mpi"

# LOADING GROMACS
echo "----- LOADING GROMACS ------"

## LOADING GROMACS
module load intel/18.0.2
module load impi/18.0.2
module load gromacs/2016.4
###SERVER_SPECIFIC_COMMANDS_END

## DEFINING RUN SCRIPT
run_script="./_RUNSCRIPT_"

## RUNNING COMMANDS
bash "${run_script}" "${num_cores}" \
                     "${mdrun_command}" \
                     "${mdrun_command_suffix}"
                     