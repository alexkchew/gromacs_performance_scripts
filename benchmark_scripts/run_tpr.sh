#!/bin/bash

# run_tpr.sh
# This script is a general run script that is used for benchmarking. 
# VARIABLES:
#	$1: number of cores
#	$2: md run commmand, e.g.:
#		gmx mdrun -nt
#		ibrun -np num_cores mdrun_mpi
#	$3: gromacs command, e.g.:
#		gmx
#		gmx_mpi
#	$4: gromacs suffix -- use this if you want to add any additional detial to mdrun. 
#

## VARIABLES THAT ARE EDITED:
#	_MDPFILE_ <-- mdp file that will be used
#	_OUTPUTPREFIX_ <-- output prefix that is used as an input, e.g. output_prefix.gro / output_prefix.top
#	_MAXWARN_ <-- number of maxwarns before grompp yells at you

# Written by: Alex K. Chew (04/10/2020)

## DEFINING VARIABLES
num_cores="${1:-28}"

## DEFINING GMX MDRUN
mdrun_command="${2-gmx mdrun -nt}"
# mdrun_command="ibrun -np ${num_cores} mdrun_mpi"

## DEFINING MDRUN COMMAND SUFFIX
mdrun_command_suffix="${3:-""}"

## RUN COMMAND
prod_run_command="${mdrun_command} ${num_cores} ${mdrun_command_suffix}"

## PRINTING RUN COMMAND
echo "${prod_run_command}"

###########################
### RUNNING GROMACS JOB ###
###########################

## DEFINING OUTPUT PREFIX
output_prefix="_OUTPUTPREFIX_"

## DEFINING MAX WARN
max_warn="_MAXWARN_"

## DEFINING SIM PREFIX
sim_prefix="${output_prefix}"

## CHECKING IF GRO EXISTS
if [ ! -e "${sim_prefix}.gro" ]; then
    ## SEEING IF XTC EXISTS
    if [ -e "${sim_prefix}.xtc" ]; then
            ## RESTARTING
            ${prod_run_command} -v \
                        -s "${sim_prefix}.tpr" \
                        -cpi "${sim_prefix}.cpt" \
                        -append \
                        -deffnm "${sim_prefix}" \
                        -px "${sim_prefix}_pullx.xvg" \
                        -pf "${sim_prefix}_pullf.xvg" 
    else
        ## RUNNING JOB
        ${prod_run_command} -v -deffnm "${sim_prefix}"
    fi
fi

