#!/bin/bash

# benchmark_input_vars.sh
# This script contains necessary input varaibles for benchmarking inputs. 
# Please fill out this code accordingly so it is clear what your input variables are.
 
## TPR FILE
BENCHMARK_TPR_PATH="/home/akchew/scratch/nanoparticle_project/nplm_sims/20200124-US-sims_NPLM_reverse_stampede/USrev-1.5_5_0.2-pullnplm-1.300_5.000-0.0005_2000-DOPC_196-EAM_2_ROT012_1/4_simulations/5.500/nplm_prod.tpr"
# Include the full path to your tpr file. The assumption is that 
# you have a tpr file that you want to test the benchmarking for.

## NUMBER OF NODES (separated by commas)
BENCHMARK_NUM_NODES="1,2,3,4,5,6"
# If multiple nodes, separate number of nodes by commas, e.g.
# NUM_NODES=1,2,3,4,5

## NUMBER OF CORES (separated by commas)
BENCHMARK_NUM_CORES="48"
# If you want to keep the cores fixed and vary the number of nodes, 
# simply put one value here, e.g. NUM_CORES=28

## SIMULATION TIME IN PICOSECONDS
BENCHMARK_SIM_TIME_PS="2000"
# Simulation time dore the debugging in picoseconds. 
# The code will use convert-tpr to edit the simulation times. 

## GROMACS COMMAND
BENCHMARK_GROMACS_COMMAND="gmx"
# This is the gromacs command that should be enabled. If this 
# does not work (e.g. gmx --version), then the benchmarking
# code cannot convert tpe files. 

## SERVER DETAILS
BENCHMARK_TIME_ALLOTTED="24:00:00"
# time allotted in days-hours:min:sec

## EMAIL ADDRESS (WHICH ALL DETAILS ARE SENT TO FOR THE JOB)
BENCHMARK_EMAIL_ADDRESS="akchew@wisc.edu"
# e.g. email@google.com
# By putting your email, your benchmark jobs will tell you when it is finished
# To edit the default email, please edit gromacs_performance_scripts/bashrc.sh, "DEFAULT_EMAIL"
# Otherwise, no email will be sent out

## DEFINING SUBMIT SCRIPT
BENCHMARK_SUBMIT_SCRIPT="submit_STAMPEDE2_skylake.sh"
# This is the submission script that benchmark will use to generate the submit files. 
# You can check all available benchmarking scripts by: 'benchmark_list_submit'
# Then, copy a submission script using 'benchmark_copy_submit'
# e.g. benchmark_copy_submit submit_SWARM.sh
