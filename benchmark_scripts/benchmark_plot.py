#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
benchmark_plot.py

This plots the benchmark results after running 'extract_gromacs_stats', e.g.
> extract_gromacs_stats benchmark ./output.csv

Written by: Alex K. Chew (04/14/2020)

"""
import sys
import os
import numpy as np
import pandas as pd

## TO AVOID ERROR WITH TINKER
import matplotlib
matplotlib.use('Agg')

import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
import math

## DEFINING DEFAULT FIGURE SIZE
FIGURE_SIZE=(8.55, 6.4125) # Landscape, 4:3 ratio

### FUNCTION TO SET MPL DEFAULTS
def set_mpl_defaults():
    ''' 
    This function sets all default parameters 
    # https://matplotlib.org/tutorials/introductory/customizing.html
    '''
    import matplotlib as mpl
    mpl.rcParams['lines.linewidth'] = 2
    mpl.rcParams['axes.linewidth'] = 1
    mpl.rcParams['axes.labelsize'] = 10
    mpl.rcParams['xtick.labelsize'] = 8
    mpl.rcParams['ytick.labelsize'] = 8
    ## EDITING TICKS
    mpl.rcParams['xtick.major.width'] = 1.0
    mpl.rcParams['ytick.major.width'] = 1.0
    
    ## FONT SIZE
    mpl.rcParams['legend.fontsize'] = 8
    
    ## CHANGING FONT
    mpl.rcParams['font.sans-serif'] = "Arial"
    mpl.rcParams['font.family'] = "sans-serif"
    ## DEFINING FONT
    font = {'size'   : 10}
    mpl.rc('font', **font)
    return

## FUNCTION TO CONVERT FIGURE SIZE
def cm2inch(*tupl):
    inch = 2.54
    if isinstance(tupl[0], tuple):
        return tuple(i/inch for i in tupl[0])
    else:
        return tuple(i/inch for i in tupl)
    
### FUNCTION TO SAVE FIGURE AS A PNG
def save_fig_png(fig, label, save_fig = True, dpi=600, bbox_inches = 'tight'):
    '''
    The purpose of this function is to save figure as a png
    INPUTS:
        fig: Figure
        label: The label you want to save your figure in
        save_fig: True or False. If True, then save the figure
        dpi: [OPTIONAL, default = 600] resolution of image
        bbox_inches: [OPTIONAL, default='tight'] how tight you want your image to be
    OUTPUTS:
        png figure
    '''
    if save_fig is True:
        label_png = label + '.png'
        ## SAVING FIGURE
        fig.savefig( label_png, format='png', dpi=dpi, bbox_inches=bbox_inches)
        ## PRINTING
        print("EXPORTING TO: %s"%(label_png))
    return
    
## FUNCTION TO CREATE FIG BASED ON CM
def create_fig_based_on_cm(fig_size_cm = (16.8, 16.8)):
    ''' 
    The purpose of this function is to generate a figure based on centimeters 
    INPUTS:
        fig_size_cm: [tuple]
            figure size in centimeters 
    OUTPUTS:
        fig, ax: 
            figure and axis
    '''
    ## FINDING FIGURE SIZE
    if fig_size_cm is not None:
        figsize=cm2inch( *fig_size_cm )
    ## CREATING FIGURE
    fig = plt.figure(figsize = figsize) 
    ax = fig.add_subplot(111)
    return fig, ax


### FUNCTION TO FIND ORDER OF MAGNITUDE
def orderOfMagnitude(number):
    return math.floor(math.log(number, 10))

### FUNCTION THAT TRUNCATES IN DECIMAL PLACE
def truncate(n, decimals=0):
    multiplier = 10 ** decimals
    return int(n * multiplier) / multiplier

### FUNCTION TO FIND FREQUENCY OF ARRAY
def find_freq_of_array(array,
                       increment_value = 5):
    '''
    This function finds the frequency outputted for an axis. It is useful to 
    use the range of the array, compute some order of magnitude, then try to 
    see if we could find a good frequency of the axis to print.
    INPUTS:
        array: [np.array]
            array that you want to find frequency for   
        increment_value: [int]
            some increment value, e.g. 2, 20, 200, etc.
    OUTPUTS:
        freq: [int] 
            some frequency in the magnitude of the increment times 10
    '''
    ## FINDING RANGE
    current_range = np.ptp(array)
    
    ## GETTING ORDER OF MAGNITUIDE
    magnitude = orderOfMagnitude(current_range)
    
    ## FIXING FOR 0 MAGNITUDE
    if magnitude == 0:
        freq = 1
    elif magnitude < 0:
        freq = truncate(current_range, np.abs(magnitude))
    else:    
        ## GETTING FREQUENCY
        freq = increment_value * 10 ** (magnitude - 1)
    
    return freq


### FUNCTION TO PLOT PERFORMANCE VS. X
def plot_perf_vs_x(x,
                   y,
                   x_label = "Number of cores",
                   y_label = "Performance (ns/day)",
                   ):
    
    '''
    This function plots the performance vs. x
    INPUTS:
        x: [np.array]
            x could be num cores, nodes ,etc.
        y: [np.array]
            performance in ns/day
    '''

    ## CREATING FIGURE AND AXIS
    fig, ax = create_fig_based_on_cm(fig_size_cm=FIGURE_SIZE)
    
    ## ADDING AXIS LABELS
    ax.set_xlabel(x_label)
    ax.set_ylabel(y_label)
    
    ## SORTING BASED ON X
    idx_sorted = np.argsort(x)
    
    ## SORTED
    x_sorted = x[idx_sorted]
    y_sorted = y[idx_sorted]
    
    ## PLOTTING
    ax.plot(x_sorted, y_sorted, linestyle='-', color = 'k')
    
    ## FINDING FREQUENCY        
    x_freq = find_freq_of_array(array = x_sorted)
    y_freq = find_freq_of_array(array = y_sorted)
    
    ## SETTING X TICKS
    xticks = np.arange(np.min(x_sorted) - x_freq, np.max(x_sorted) + 2*x_freq, x_freq) 
    ax.set_xticks(xticks)
    ## SETTING X LIM
    ax.set_xlim([xticks[0], xticks[-1]])
    
    ## SETTING Y TICKS
    if np.max(y_sorted) > 1:
        yticks = np.arange( int(np.min(y_sorted)) - y_freq, int(np.max(y_sorted)) + y_freq*2, y_freq) 
    else:
        y_min = np.min(y_sorted)
        y_max = np.max(y_sorted)
        yticks = np.arange( y_min - y_freq, y_max + y_freq*2, y_freq) 
        
    ax.set_yticks(yticks)
    ## SETTING X LIM
    ax.set_ylim([yticks[0], yticks[-1]])
    
    ## SETTING Y LABELS TO SCIENTIFIC
    if np.max(y_sorted) < 1:
        ax.ticklabel_format(axis = "y", style = "sci",  scilimits=(0,0))
        ax.yaxis.set_major_formatter(mtick.FormatStrFormatter('%.2e'))
    
    ## FINDING MAXIMA
    maxima_x = x_sorted[np.argmax(y_sorted)]
    
    ## PLOTTING
    ax.axvline(x = maxima_x, linestyle='--', color = 'r', label = 'Max perf.')
    
    ## ADDING LEGEND
    ax.legend()
    
    ## SETTING FIG TIGHT LAYOUT
    fig.tight_layout()
    
    return fig, ax


## SETTING MPL DEFAULTS
set_mpl_defaults()

## DEFINING IF YOU WANT TO SAVE FIG
SAVE_FIG=True


#%% MAIN SCRIPT
if __name__ == "__main__":
    
    ## DEFINING PATH TO EXTRACT CSV
    path_to_csv=sys.argv[1]
    # path_to_csv=r"/Volumes/akchew/scratch/nanoparticle_project/nplm_sims/benchmarking_stampede/benchmarking/output.csv"
    # r"/Volumes/akchew/scratch/nanoparticle_project/nplm_sims/benchmarking/benchmarking/output.csv"
    
    ## DEFINING OUTPUT PATH
    path_output = os.path.dirname(path_to_csv)
    
    ## LOADING CSV
    data = pd.read_csv(path_to_csv, skiprows=1)
    
    ## GETTING PERFORMANCE
    perf = np.array(data['performance'])
    
    ## GETTING NUM NODES AND CORES
    num_nodes = np.array(data['num_nodes'])
    num_cores = np.array(data['num_cores'])
    
    ## DEFINING LOGICALS
    vary_nodes = np.any(np.diff(num_nodes))
    vary_cores = np.any(np.diff(num_cores))
    
    ## NUMBER OF CORES WAS VARIED
    if vary_cores == True:
        ## PLOTTING FIG
        fig, ax = plot_perf_vs_x(x = num_cores,
                                 y = perf,
                                 x_label = "Number of cores",
                                 y_label = "Performance (ns/day)",
                                 )
        
        ## STORING FIG
        save_fig_png(fig = fig, 
                     label = os.path.join(path_output,
                                          "perf_vs_num_cores"
                                          ), 
                     save_fig = SAVE_FIG, dpi=300, bbox_inches = 'tight')
        
        ## NORMALIZE BY CORES
        fig, ax = plot_perf_vs_x(x = num_cores,
                                 y = perf / num_cores,
                                 x_label = "Number of cores",
                                 y_label = "Performance (ns/day/core)",
                                 )
        
        ## STORING FIG
        save_fig_png(fig = fig, 
                     label = os.path.join(path_output,
                                          "norm_perf_vs_num_cores"
                                          ), 
                     save_fig = SAVE_FIG, dpi=300, bbox_inches = 'tight')
        
    ## IF NUMBER OF NODES WAS VARIED
    if vary_nodes == True:
        ## PLOTTING FIG
        fig, ax = plot_perf_vs_x(x = num_nodes,
                                 y = perf,
                                 x_label = "Number of nodes",
                                 y_label = "Performance (ns/day)",
                                 )
        
        ## STORING FIG
        save_fig_png(fig = fig, 
                     label = os.path.join(path_output,
                                          "perf_vs_num_nodes"
                                          ), 
                     save_fig = SAVE_FIG, dpi=300, bbox_inches = 'tight')
        
        
        ## NORMALIZE BY CORES
        fig, ax = plot_perf_vs_x(x = num_nodes,
                                 y = perf / num_nodes,
                                 x_label = "Number of nodes",
                                 y_label = "Performance (ns/day/node)",
                                 )
        
        ## STORING FIG
        save_fig_png(fig = fig, 
                     label = os.path.join(path_output,
                                          "norm_perf_vs_num_nodes"
                                          ), 
                     save_fig = SAVE_FIG, dpi=300, bbox_inches = 'tight')

    
    
    
    