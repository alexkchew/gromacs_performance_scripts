#!/bin/bash

# extract_gromacs_stats.sh
# The purpose of this script is to extract the GROMACS 
# stats in terms of performance and number of atoms, etc.
# 
# Written by: Alex K. Chew (03/31/2020)
# 
# INPUTS:
#	$1: sim_prefix
#		prefix of your simulation, usually ends with "_prod" for production
#	$2: output_file
#		output csv file to print the information
#	$3: output_type
#		type of output, either the following
#			'overwrite': overwrites current csv
#			'append' : adds to current csv
# 

### FUNCTION TO JOIN ARRAY TO STIRNG
join_array_to_string () 
{ 
    local IFS="$1";
    shift;
    echo "$*"
}

######################
### INPUT VARABLES ###
######################

## FINDING DATE
current_date=$(date '+%Y-%m-%d')

## DEFINING PREFIX
sim_prefix="${1-nplm_prod}"

## DEFINING OUTPUT FILE
output_file="${2-""}"

## DEFINING DEFAULT
if [ -z "${output_file}" ]; then
	output_file="${PREF_PATH_OUTPUT_DIR}/${current_date}-stats.csv"
fi

## DEFINING TYPE
output_type="${3-overwrite}"

## DEFINING PATH TO LOOK INTO
path_of_origin="$(pwd)"

## DEFINING LOG AND GRO FILE
log_file="${sim_prefix}.log"

##############
### HEADER ###
##############

## DEFINING COMMA SEPARATED HEADING
header=${path_of_origin} # first row of csv file
heading="Relative path,hostname,num_atoms,performance,walltime,num_nodes,num_cores,gromacs_version" # second row of csv file

## MAKING DIRECTORY IF NOT AVAILABLE
output_file_dir=$(dirname ${output_file})
if [ ! -e "${output_file_dir}" ]; then
	echo "Creating output directory"
	echo "${output_file_dir}"
	mkdir -p "${output_file_dir}"
fi

## GETTING OUTPUT TYPE
if [[ "${output_type}" == "overwrite" ]]; then
	echo "${header}" > "${output_file}"
elif [[ "${output_type}" == "append" ]]; then
	echo "${header}" >> "${output_file}"
else
	echo "Error! Output type not defined: ${output_type}"
	echo "Available types: overwrite, append"
	exit 1
fi

## ADDING HEADING
echo "${heading}" >> "${output_file}"

###################
### MAIN SCRIPT ###
###################

## FINDING ALL LOG FILES
read -a log_file_array <<< $(find "${path_of_origin}" -name "${log_file}")

## PRINTING
echo "============ LOG FILES (Looking for: ${log_file}) ============"
idx=0
## LOOPING THROUGH LOG FILE
for each_log_file in ${log_file_array[@]}; do
	if [[ ${idx} == 0 ]]; then
		echo "ORIGIN: ${path_of_origin}"
		idx=$((${idx}+1))
	fi
	## GETTING NAME WITHOUT THE ORIGIN
	name_without_origin=$(echo ${each_log_file} | sed "s#${path_of_origin}##g")

	echo "   --> ${name_without_origin}"

	## GETTING HOST NAME
	hostname=$(grep -E "Host:" ${each_log_file} | awk '{print $2}' | tail -n1)

	## GETTING NUMBER OF ATOMS
	num_atoms=$(grep "There are:" "${each_log_file}" | awk '{print $3}' | tail -n1)

	## GETTING PERFOMRANCE
	performance=$(tail "${each_log_file}" | grep "Performance" | awk '{print $2}' | tail -n1) # ns/day
	walltime=$(grep "Time:" "${each_log_file}" | tail -n 1 | awk '{print $3}' | tail -n1) # seconds

	## NUMBER OF CORES
	num_nodes=$(grep "Running on" ${each_log_file} | awk '{print $3}' | tail -n1)
	# num_cores=$(grep "Running on" ${each_log_file} | awk '{print $7}' | tail -n1) <-- fails for different num cores

	perc_num_cores=$(tail -n10 ${each_log_file}  | grep "Time:" | awk '{print $NF}')
	num_cores_float=$(bc -l <<< "${perc_num_cores} / 100")
	num_cores=${num_cores_float%.*} # integer

	## GETTING GROMACS VERSION
	gromacs_version=$(grep "GROMACS version" ${each_log_file} | awk '{print $NF}' | tail -n1)

	## OUTPUT ARRAY
	declare -a output_array=("${name_without_origin}" \
							"${hostname}" \
							"${num_atoms}" \
							"${performance}" \
							"${walltime}" \
							"${num_nodes}" \
							"${num_cores}" \
							"${gromacs_version}"
							)
	## PRINTING
	echo -e "      Hostname: ${hostname} // Number atoms: ${num_atoms} // Perf (ns/day): ${performance}\n" 

	## DEFINING INFORMATION STRING
	output_string="$(join_array_to_string ", " "${output_array[@]}")"

	## SAVE TO FILE
	echo "${output_string}" >> "${output_file}"

done

## ADDING EMPTY LINE
echo "" >> "${output_file}"

## PRINTING
echo "-------------------------------------"
echo "Complete! See output using:"
echo "less ${output_file}"

# echo "------------- First 5 lines -----------------------"
# head -n5 "${output_file}"
# echo "------------- Last 5 lines -----------------------"
# tail -n5 "${output_file}"







