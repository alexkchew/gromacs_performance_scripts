#!/bin/bash

# bashrc.sh
# This script contains all variables and functions used 
# to observe the performance of GROMACS
# 
# Written by: Alex K. Chew (03/31/2020)
# 
# USAGE:
# 	source bashrc.sh
#	Use full path:
#		source /home/akchew/bin/bashfiles/gromacs_performance_scripts/bashrc.sh
# VARIABLES:
#	PERF_SCRIPT_PATH:
#		Path to these scripts
#
#
# FUNCTIONS:
#	extract_gromacs_stats:
#		extracts gromacs performance information
#	benchmark_gromacs:
#		benchmarks the gromacs
#	copy_top_include_files:
#		copies all topology information into an output folder
#	mdp_find_option_values:
#		finds mdp option
#	benchmark_print_input_vars:
#		prints the benchmark input variables
#	_import:
#		"sources" bash function within another function
#	str2array_by_delim:
#		converts string to array by a deliminator

## DEFINING DEFAULT EMAIL
DEFAULT_EMAIL="${NETID}@wisc.edu"
# Make sure to edit this email when you are trying to run benchmarking functions!

## DEFINING CURRENT PATH TO SCRIPTS
PERF_SCRIPT_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

## DEFINING PATH TO GROMACS STATS
PERF_PATH_TO_GROMACS_STATS="${PERF_SCRIPT_PATH}/extract_gromacs_stats.sh"

## PATH TO BENCHMARK SCRIPTS
export PERF_PATH_TO_BENCHMARK_DIR="${PERF_SCRIPT_PATH}/benchmark_scripts"
export PERF_PATH_TO_BENCHMARK_SUBMITSCRIPTS="${PERF_PATH_TO_BENCHMARK_DIR}/submit_scripts"

## PATH TO GROMACS BENCHMARK
PERF_PATH_TO_GROMACS_BENCHMARK="${PERF_SCRIPT_PATH}/benchmark_gromacs.sh"



## PATH TO GROMACS BENCHMARK INITIAL SCRIPT
PERF_PATH_TO_GROMACS_BENCHMARK_INITIAL="${PERF_PATH_TO_BENCHMARK_DIR}/benchmark_input_vars.sh"

## DEFINING PATH TO PYTHON
PERF_PATH_TO_GROMACS_BENCHMARK_PYTHON="${PERF_PATH_TO_BENCHMARK_DIR}/benchmark_plot.py"

## DEFINING OUTPUT DIRECTORY
export PREF_PATH_OUTPUT_DIR="${PERF_SCRIPT_PATH}/output_csv"

### FUNCTION TO EXTRACT GROMACS STATS
function extract_gromacs_stats () {

	bash "${PERF_PATH_TO_GROMACS_STATS}" $@

}

### FUNCTION TO INITIATE BENCHMARK
function benchmark_start () {
	## DEFINING CURRENT PATH
	current_path=${1:-$(pwd)}

	## INITIATE BENCHMARK
	echo "~~~~ Benchmarking commence! ~~~~~"

	## DEFINING INITIAL FILE
	initial_file_="$(basename "${PERF_PATH_TO_GROMACS_BENCHMARK_INITIAL}")"
	pwd
	echo "Copying initial file: ${initial_file_} to current working directory"

	## CHECKING IF YOU WANT TO OVERWRITE
	if [ -e "${current_path}/${initial_file_}" ]; then
		echo "Warning, ${initial_file_} exists!"
		echo "Will overwrite in 3 seconds, ctrl + c if this is incorrect!"
		sleep 3
	fi
	## COPYING
	cp -r "${PERF_PATH_TO_GROMACS_BENCHMARK_INITIAL}" "${current_path}/${initial_file_}"

	## PRINTING CURRENT FILE
	echo "Please edit ${initial_file_} before continuing!"
	echo "Edit with vim: vim ${initial_file_}"
	echo "Edit with EMACS: emacs ${initial_file_}"
	echo "Adding current default email: ${DEFAULT_EMAIL}"
	echo "If this is not the default, please edit it in gromacs_performance_scripts/bashrc.sh"

	## EDITING
	sed -i "s#\<_EMAIL_\>#${DEFAULT_EMAIL}#g" "${current_path}/${initial_file_}"
}

### FUNCTION TO LIST SUBMIT SCRIPTS
function benchmark_list_submit () {
	echo "Benchmark submit path: ${PERF_PATH_TO_BENCHMARK_SUBMITSCRIPTS}"
	echo "Listing submission scripts within benchmark:"
	ls "${PERF_PATH_TO_BENCHMARK_SUBMITSCRIPTS}" -1
}

### FUNCTION TO COPY BENCHMARK
function benchmark_copy_submit () {
	## DEFINING SUBMIT FILE
	submit_file="$1"
	## DEFINING OUTPUT LOCATION
	output_location="${2-$(pwd)}"

	echo "Copying ${submit_file} to current directory"
	path_submit_file="${PERF_PATH_TO_BENCHMARK_SUBMITSCRIPTS}/${submit_file}"
	if [ ! -e "${path_submit_file}" ]; then

		echo "Error! Submission file is not available at path:"
		echo "${path_submit_file}"
		echo "Check benchmark list by using 'benchmark_list_submit'"
		echo "If you want to add your submission script, use 'benchmark_save_submit your_submit_file_name.sh'"
	else
		cp "${path_submit_file}" "${output_location}"
	fi

}

### FUNCTION TO SUBMIT A COPY TO BENCHMARK
function benchmark_save_submit () {
	## STORING SUBMIT FILE
	submit_file="$1"
	echo "Copying ${submit_file} to benchmark submit scripts"
	echo "Check with 'benchmark_list_submit' function to see if it is saved correctly"
	cp "${submit_file}" "${PERF_PATH_TO_BENCHMARK_SUBMITSCRIPTS}"


}

### FUNCTION TO PREPARE BENCHMARK
function benchmark_gromacs () {
	bash "${PERF_PATH_TO_GROMACS_BENCHMARK}" $@
}

### FUNCTION TO COPY ALL INCLUDE FILES
# The purpose of this script is to copy all topology files for GROMACS. This is 
# useful because we do not always want to copy all the files, only the files 
# necessary for the topology. This code will look into the top file and copy any 
# "#include" flags into the current directory. 
# INPUTS:
#	$1: topology file
#	$2: output location to copy the files
# OUTPUTS:
#	This function will copy the topology and all necessary files to output location
# USAGE EXAMPLE:
#	copy_top_include_files nplm.top ./testing
function copy_top_include_files () {
	## DEFINING INPUTS
	top_file_="$1"
	output_location_="${2-$(pwd)}"

	## DEFINING TOP LOCATION
	top_location=$(dirname ${top_file_})

	## MAKING SURE DIR LOC EXISTS
	mkdir -p "${output_location_}"

	## FINDING ALL INCLUDE LOCATIONS
	while IFS= read -r line; do
		## DEFINING SPECIFIC FILE
		specific_file="$(echo "${line}" | grep -o '".*"' | sed 's/"//g')"

		## SEEING IF STRING IS A PATH
		if [[ "${specific_file}" == *\/* ]] || [[ "${specific_file}" == *\\* ]]; then
			## DEFINING DIRECTORY
			file_name=$(dirname ${specific_file})
		else
			file_name=${specific_file}
		fi

		## COPYING FILE
		if [[ ! -e "${output_location_}/${file_name}" ]]; then
			## PRINTING
			echo "top_include: Copying ${file_name} -> ${output_location_}"
			## COPYING
			cp -r "${top_location}/${file_name}" "${output_location_}"
		fi

		# awk "{print $2}"

	done < <(grep "^#include" ${top_file_})

}

### FUNCTION TO FIND MDP OPTIONS
# The purpose of this function is to find mdp options. We are assuming here that you have some 'options = value', where value represents the third column via awk
# INPUTS:
#   $1: mdp file
#   $2: variable that you are looking for (e.g. nsteps)
# OUTPUTS:
#   Values that corresonds to options.
# USAGE:
#   mdp_nsteps=$(mdp_find_option_values ${mdp_file} "nsteps")
function mdp_find_option_values () {
    ## DEFINING INPUTS
    input_mdp_file_="$1"
    input_mdp_options_="$2"
    
    ## USING GREP TO FIND THE FINAL VALUE
    echo "$(grep "$2" "$1"  | awk '{print $3}')"
    
}

### FUNCTION THAT UPDATES MDP FILE
# This function edits the mdp options. 
# INPUTS:
#	$1: mdp file
#	$2: variables that you are trying to eidt
#	$3: new value that you want
# OUTPUTS:
#	mdp file with updated value
function mdp_edit_option_values () {
    ## DEFINING INPUTS
    input_mdp_file_="$1"
    input_mdp_options_="$2"
    new_mdp_value="$3"

	sed -i "s/^${input_mdp_options_}.*\=.*/${input_mdp_options_}  =/  ${new_mdp_value}" "${input_mdp_file_}"
	## PRINTING
	echo "Editing ${input_mdp_file_}: ${input_mdp_options_} -> ${new_mdp_value}"
	# sed 's/^dt.*\=.*/dt=5/g' benchmark_prod.mdp 
}

### FUNCTION TO SOURCE FILES WITHIN FUNCTIONS
function _import {
  local -r file="$1"
  shift
  source "$file" "$@"
}

### FUNCTION TO PRINT OUTPUT FOR BENHCMARK INPUTS
# This function prints the input variables for the benchmarking inputs
# INPUTS:
#	$1: benchmarking file 
# OUTPUTS:
#	this function will simply print the outputs of the file
function benchmark_print_input_vars () {
	## DEFINING INPUT FILE
	input_file_="${1-benchmark_input_vars.sh}"

	## LOADING BY SOURCE
	_import "${input_file_}" 
	# source "${input_file_}"

	echo "==== ${input_file_} ===="
	echo "Number of cores: ${BENCHMARK_NUM_CORES}"
	echo "Number of nodes: ${BENCHMARK_NUM_NODES}"
	echo "Simulation time (ps): ${BENCHMARK_SIM_TIME_PS}"
	echo "Gromacs command: ${BENCHMARK_GROMACS_COMMAND}"
	echo "TPR file: ${BENCHMARK_TPR_PATH}"

}

### FUNCTION TO CONVERT STRING TO ARRAY
str2array_by_delim () 
{ 
    input_string="$1";
    deliminator="${2-_}";
    IFS="${deliminator}" read -ra converted_array <<< "${input_string}";
    echo "${converted_array[@]}"
}

### FUNCTION TO SUBMIT JOBS
# This function simply submits the job given a submit script
# To submit a job without some nodes: benchmark_submit_jobs 1 job_list.txt "--exclude=swarm[010-014]"
function benchmark_submit_jobs () {
	## DEFINING NUMBER OF JOBS
	num_jobs_="$1"
	## DEFINING INPUT SUBMIT SCRIPT
	input_submit_script_="${2-job_list.txt}"
	## DEFINING ADDITIONAL COMMANDS
	additional_sbatch_commands="${3-""}"
	## DEFINING JOB COMPLETE
	job_completed_file="job_completed.txt"

	## DEFINING POINT OF ORIGIN
	point_of_origin_="$(pwd)"

	## CHECKING IF SELECTION IS NOTHING
	if [ -z "${num_jobs_}" ];then
	    echo "Error! Specify how many jobs you want to submit!"
	    echo "e.g. benchmark_submit_jobs 5 job_list.txt"
	    exit
	fi

	# DEFINING COUNTER VARIABLE
	currentCounter=0

	# Now, going to job_list_file and running the job
	while [ "${currentCounter}" -lt "${num_jobs_}" ]; do

	    ## READING THE FIRST LINE
	    firstLine=$(head -1 "${input_submit_script_}")
		
	    # Check if the first line is there
	    if [ -z "${firstLine}" ]; then
	        echo "No more jobs in ${input_submit_script_}"
	        echo "Stopping script here!"
	        break
	    fi

	    # Printing
	    echo "RUNNING SLURM JOBS"

	    ## GETTING BASENAME
	    directory_path="$(dirname ${firstLine})"
	    submit_file="$(basename ${firstLine})"

	    ## GOING INTO DIRECTORY
	    cd "${directory_path}"

	    ## SUBMITTING
	    if [ -z "${additional_sbatch_commands}" ]; then
    		sbatch "${submit_file}"
	    else
	    	sbatch "${additional_sbatch_commands}" "${submit_file}"
		fi

	    ## GOING BACK TO ORIGIN
	    cd "${point_of_origin_}"

	    ## COPYING FIRST LINE
	    echo "${firstLine}" >> "./${job_completed_file}"

	    ## REMOVING FIRST LINE
	    sed -i "1d" "./${input_submit_script_}"

	    ## ADDING TO COUNTER
	    currentCounter=$(( ${currentCounter} + 1 ))
	done

# PRINTING SUMMARY #
echo "----- SUMMARY -----"
echo "Ran ${num_jobs_} jobs"
echo "Job list: ${input_submit_script_}"
echo "Job completed: ${job_completed_file}"

}

### FUNCTION TO PRINT PNG
# This function takes in the extract stats and then prints png for it
# Example: benchmark_print_png ./output.csv python3.6
function benchmark_print_png () {
	input_file_="${1-./output_csv}"
	python_func="${2-python}"

	## SEEING IF INPUT FILE EXISTS
	if [ ! -e "${input_file_}" ]; then
		echo "Error, csv file does not exist: ${input_file_}"
		echo "Make sure to run extraction csv stats, e.g."
		echo "> extract_gromacs_stats benchmark ./output.csv"
		echo "Stopping here!"
		return 1
	fi

	## RUNNING PYTHON
	"${python_func}" "${PERF_PATH_TO_GROMACS_BENCHMARK_PYTHON}" "${input_file_}"

}


## EXPORTING FUNCTIONS
export -f copy_top_include_files \
		  mdp_find_option_values \
		  mdp_edit_option_values \
		  benchmark_print_input_vars \
		  _import \
		  str2array_by_delim \
		  benchmark_submit_jobs
