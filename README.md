# gromacs_performance_scripts
The purpose of these scripts are to evaluate the performance of GROMACS jobs. These scripts were written in pure bash shell so that it is transferable to Unix systems. It simply looks for `*.log` files and extracts information from them. This script also has code to initiate benchmarking procedures to see how well your computing configuration (e.g. number of cores) perform.

# Description of files
- `bashrc.sh`: Has all global functions and variables
- `extract_gromacs_stats.sh`: Main function that runs the extraction protocol
- `output_example`: Folder with example csv of the output when using the `extract_gromacs_stats` function. 
- `benchmark_gromacs.sh`: Script that runs the main benchmarking code
- `benchmark_scripts`: Directory containing all benchmarking scripts

# Usage
Step 1: Clone this .git file
> git clone https://gitlab.com/alexkchew/gromacs_performance_scripts.git

Step 2: Load the rc file
> source gromacs_performance_scripts/bashrc.sh

Step 3: Go to directory where your simulations are stored and run the extract stats functions
> extract_gromacs_stats nplm_prod ./output.csv append

`extract_gromacs_stats` has three inputs:
- Prefix of your simulation.
- Output CSV. If you write "", it will store within the `gromacs_performance_scripts/output_csv` folder.
- "append" or "overwrite", which it will either add to an existing CSV file or remove it.

# Output example

Below is an example of the output csv image. 

![Output example CSV](output_example/output_example.png)

Description:

- The first line is the current path you ran the `extract_gromacs_stats` command
- Relative path: Path to the log file
- hostname: Hostname that performed your job. This is useful to seeing which server you ran the job.
- num_atoms: Number of atoms in your simulation.
- performance: job performance in ns/day.
- walltime: total walltime your job took in seconds.
- num_nodes: total number of nodes the job used. 
- num_cores: total number of cores the job used. 

For example, row 3 shows the job ran in the STAMPEDE2 cluster with 6 nodes, 408 total cores, and achieving a performance of 29.3 ns/day. 

# GROMACS Benchmarking

Benchmarking is critical for making sure you are getting the best performance for a GROMACS job. Typically, benchmarking is done by varying the number of cores (or similarly, the number of nodes). The ideal output is shown below:

| ![benchmark_example](benchmark_scripts/image_examples/perf_vs_num_nodes.png) |
|:--:|
| *Example of performance for a GROMACS job on XSEDE STAMPEDE2 (Knights Landing cluster) for a 225,696 atom system* |

This image shows the performance in ns/day versus the number of nodes used for XSEDE STAMPEDE2 cluster. The overall performance generally increases with essentially more cores (68 cores per node in this example) as a result of increased parallelization. 

## Requirements
To generate benchmarking information for your system, you will need the following:
- TPR file: This file contains all the information required to run your job using `gmx grompp` commands. Please review GROMACS tutorials if you do not know how to generate a tpr file (e.g. Justin Lemkul's GROMACS tutorials: http://www.mdtutorials.com/gmx/)
- Submit file: Submission file that you use to run your jobs in the cluster. This code has examples of submission scripts, which you can modify for your own work. 

Software requirements:
- GROMACS Version 2016 (or higher)
- SLURM submission system
- Python 3.6 (optional)

## Procedure

### 1. Initiate the benchmarking
Make sure that you have loaded the bashrc in `gromacs_performance_scripts/bashrc.sh` (see Usage above)

Start by creating a directory for your benchmarking, e.g.
> mkdir -p "benchmarking" \
> cd "benchmarking"

Initiate the benchmarking by running:
> benchmark_start

This will create a file `benchmark_input_vars.sh`, which you modify with your tpr file information, and so forth for this to generate benchmarking files. An example output is shown below when running `benchmark_start`:

![](benchmark_scripts/image_examples/benchmark_tutorial_1.png)

### 2. Edit the input file
Open the `benchmark_input_vars.sh` file with your favorite editor, then edit the following:
- `BENCHMARK_TPR_PATH`: Full path to your TPR file. The script will copy this tpr file and use it for benchmarking. If you don't have a tpr file, an example tpr file is available in `gromacs_performance_scripts/benchmark_scripts/input_examples/benchmark.tpr`
- `BENCHMARK_NUM_NODES`: Number of nodes that you want to benchmark. If you want multiple nodes, vary this by adding commas, e.g. "1,2,3" means run the job for 1, 2, and 3 nodes as separate jobs.
- `BENCHMARK_NUM_CORES`: Number of cores per node that you want to vary. Typically, this is a fixed value with the maximum cores per node. If you want to vary multiple cores, do the same procedure as `BENCHMARK_NUM_NODES`.
- `BENCHMARK_SIM_TIME_PS`: Total simulation time in picoseconds that you want each benchmark run to perform. Usually this is a small, e.g. 2000 ps, for GROMACS to optimize the threads and domain decomposition. 
- `BENCHMARK_GROMACS_COMMAND`: Command that you use to call GROMACS, e.g. `gmx`.
- `BENCHMARK_TIME_ALLOTTED`: Total time that you would allow your job to run for. This will be inputted into your submission script so your job does not exceed time constraints in a cluster. 
- `BENCHMARK_EMAIL_ADDRESS`: Please fill in your email address. This variable will be inputted to your submission script so you could get feedback on when your jobs are complete. 
- `BENCHMARK_SUBMIT_SCRIPT`: Submission script that you will use for your job. Examples of submission scripts are discussed below.

Example of the `benchmark_input_vars.sh` script is shown below:
![](benchmark_scripts/image_examples/benchmark_tutorial_2.png)

Once you are done editing, feel free to check what the inputs are using the command:
> benchmark_print_input_vars benchmark_input_vars.sh

### 3. Develop the submission script
There are example submission scripts that you could view by running:
> benchmark_list_submit

Some example submission scripts are:
- `submit_SWARM.sh`: Submission script for the Van Lehn Group (UW-Madison) SWARM cluster
- `submit_STAMPEDE2_normal.sh`: Submission script for XSEDE STAMPEDE2 in the Knights Landing cluster
- `submit_STAMPEDE2_skylake.sh`: Submission script for XSEDE STAMPEDE2 in the Skylake cluster

You could copy these scripts using the command:
> benchmark_copy_submit submit_SWARM.sh

An example of the output is shown below:
![](benchmark_scripts/image_examples/benchmark_tutorial_3.png)

An example submission script for SWARM is shown below:
| ![](benchmark_scripts/image_examples/benchmark_tutorial_3a.png) |

The main takeaways is that the submission script has variables that will be edited when generating the jobs, such as:
- `_JOBNAME_`: Name of the job
- `_NUMNODES_`: Number of nodes
- `_CORESPERNODE_`: Number of cores per node
- `_EMAIL_`: Email address
- `_TOTALCORES_`: Total number of cores for the job (Number of nodes * cosres per node)
- `_RUNSCRIPT_`: Run script that the job will perform
- `_TIMEALLOTTED_`: Total time allotted for the job

These input variables will be varied in the next step. Note that if you want to save the submission file for future runs, you could store them:
> benchmark_save_submit your_submit_name.sh

Then, you can view these submission files by running `benchmark_list_submit`. 

### 4. Generate jobs for benchmarking

Once you are finished editing your input file (`benchmark_input_vars.sh`) and the submission file, you are now ready to generate jobs for benchmarking. Run the following to create these jobs:
> benchmark_gromacs benchmarking benchmark_input_vars.sh

This function will create a directory called `benchmarking` using the input variables from `benchmark_input_vars.sh`. By running this function, it will perform the following:
- Copy tpr file to current directory.
- Edit the total simulation time for your tpr according to the `BENCHMARK_TIME_ALLOTTED` variable.
- Loop through number of nodes / cores and generate directories within `benchmarking`.
- Create a list of jobs that will need to be performed.

An example output when running the command is shown below:
![](benchmark_scripts/image_examples/benchmark_tutorial_4.png)
In this example, I am keeping the number of nodes constant while changing the number of cores. The function created 4 directories that tell me the number of nodes and number of cores, e.g. `nodes_1-corespernode_4` means 1 node and 4 cores per node. Each directory has a `*.tpr` file, `run.sh` file, and `submit.sh` file. The `run.sh` basically performs the job and `submit.sh` is the submission file. 

You will notice that a `job_list.txt` has also been created. The `job_list.txt` for this example is:
![](benchmark_scripts/image_examples/benchmark_tutorial_5.png)

### 5. Submit benchmarking jobs

After double-checking the submission scripts, you can now submit the job. 

#### Optional step:
At this point, you could `scp` the directory into another server, such as STAMPEDE2, e.g.
> cd ../ # Go back a directory \
> scp -rv "benchmarking" USERNAME@login1.stampede2.tacc.utexas.edu

Note that if you do move the script to another server, make sure to git pull and source the benchmarking functions (as described in Usage). 

Then, you could submit the jobs. Go to the directory with `job_list.txt`, then run the following:
> benchmark_submit_jobs 4 job_list.txt
`benchmark_submit_jobs` takes three variables:
- Number of jobs to submit (e.g. 4 jobs)
- Job list name (e.g. `job_list.txt`)
- Any additional slurm commands. For example, you could exclude specific nodes:
> benchmark_submit_jobs 4 job_list.txt "--exclude=swarm[010-014]"

Below is an example when running the `benchmark_submit_jobs` function.
![](benchmark_scripts/image_examples/benchmark_tutorial_6.png)

### 6. Analyze the results

After your jobs have completed, each directory in `benchmarking` should have a `benchmark.log` file. We can then use the extraction stats tool as discussed earlier by running:
> extract_gromacs_stats benchmark ./output.csv

Then, you could use python tools to plot the output. This repository has a python example:
> benchmark_print_png ./output.csv "python3.6"

`benchmark_print_png` takes two variables:
- Full path to the csv file.
- Python environment (e.g python, python3.6, etc.)

This will output 4 .pngs in the same directory:
- `norm_perf_vs_num_cores.png`: Performance (ns/day/cores) vs. number of cores
- `norm_perf_vs_num_nodes.png`: Performance (ns/day/nodes) vs. number of nodes
- `perf_vs_num_cores.png`: Performance (ns/day) vs. number of cores
- `perf_vs_num_nodes.png`: Performance (ns/day) vs. number of nodes

Examples of these images are available in `benchmark_scripts/image_examples`. An example of `norm_perf_vs_num_nodes.png` is shown below
![benchmark_example](benchmark_scripts/image_examples/norm_perf_vs_num_nodes.png)

This shows that the performance on a per-node basis is the best for 1 node, as a result of the overhead of communication between nodes for multi-node processes. The performance per node decreases with increasing number of nodes, but the overall performance is higher for larger number of nodes. In other words, using more nodes may get you faster results (i.e. higher ns/day), but it does not mean that the job is using computational resources efficiently. 



